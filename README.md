# TAD Telecom task

What I had to do: [pdf](python%20task_ENG.pdf)

Solved task in files:
1. [Time delta specifier](task_1.py)
1. [Multiplexed file-by-file line-by-line order](task_2.py)
1. [Merge function](task_3.py)

Unit tests:
1. [[Tests] Time delta specifier](task_1_test.py)
1. [[Test] Multiplexed file-by-file line-by-line order](task_2_test.py)
1. [[Tests] Merge function](task_3_test.py)
