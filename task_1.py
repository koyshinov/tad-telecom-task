import re


TIME_DELTA_SPECIFIER_TEMPLATE = re.compile(r'''
    (?P<value>
        \d*
        (?:
          \.
          \d+
        )?
    )
    (?P<specifier>
        [smhd]?
    )
    $
''', re.VERBOSE | re.IGNORECASE)

SECONDS_IN_MINUTE = 60
SECONDS_IN_HOUR = 60 * SECONDS_IN_MINUTE
SECONDS_IN_DAY = 24 * SECONDS_IN_HOUR

SECONDS_IN_SPECIFIER = dict(
    s=1,
    m=SECONDS_IN_MINUTE,
    h=SECONDS_IN_HOUR,
    d=SECONDS_IN_DAY,
)


def time_delta_specifier_to_seconds(td_spec: str) -> int:
    td_spec = td_spec.strip()

    if not td_spec:
        raise ValueError

    td_spec_match_obj = TIME_DELTA_SPECIFIER_TEMPLATE.match(td_spec)

    if not td_spec_match_obj:
        raise ValueError

    value, specifier = td_spec_match_obj.groups()
    specifier = specifier.lower()

    if specifier and not value:
        return SECONDS_IN_SPECIFIER[specifier]

    if not specifier or specifier == 's':
        return int(float(value))

    value = float(value)

    return int(value * SECONDS_IN_SPECIFIER[specifier])
