import pytest

from task_1 import time_delta_specifier_to_seconds


def test_from_technical_task_01():
    assert time_delta_specifier_to_seconds('30') == 30


def test_from_technical_task_02():
    assert time_delta_specifier_to_seconds('30s') == 30


def test_from_technical_task_03():
    assert time_delta_specifier_to_seconds('s') == 1


def test_from_technical_task_04():
    assert time_delta_specifier_to_seconds('60.5m') == 3630


def test_from_technical_task_05():
    with pytest.raises(ValueError):
        time_delta_specifier_to_seconds('10seconds')


def test_from_technical_task_06():
    with pytest.raises(ValueError):
        time_delta_specifier_to_seconds('1y')


def test_from_technical_task_07():
    with pytest.raises(ValueError):
        time_delta_specifier_to_seconds('')


def test_single_hour():
    assert time_delta_specifier_to_seconds('h') == 60*60


def test_float_seconds():
    # function must return int
    assert time_delta_specifier_to_seconds('0.8s') == 0


def test_only_fraction_part():
    assert time_delta_specifier_to_seconds('.5m') == 30


def test_comma():
    with pytest.raises(ValueError):
        # use dot instead of comma
        time_delta_specifier_to_seconds('0,5m')


def test_upper_specifier():
    assert time_delta_specifier_to_seconds('H') == 60 * 60
