def read_file_loop(file_obj):
    while True:
        for line in file_obj.readlines():
            yield line.strip()
        file_obj.seek(0)


def read_files_in_multiplex_line_by_line(*filenames: str) -> str:
    file_objs = list(open(filename) for filename in filenames)
    file_iters = list(iter(read_file_loop(file_obj)) for file_obj in file_objs)

    while True:
        for file_iter in file_iters:
            yield next(file_iter)
