def merge(*args):
    iterables = list()
    first_line = list()

    for arg in args:
        current_iter = iter(arg)
        try:
            first_line.append(next(current_iter))
            iterables.append(current_iter)
        except StopIteration:
            pass

    while True:
        if not iterables:
            break

        index, (min_value, min_iter) = min(
            enumerate(zip(first_line, iterables)),
            key=(lambda x: x[1][0])
        )
        yield min_value
        try:
            first_line[index] = next(min_iter)
        except StopIteration:
            iterables.pop(index)
            first_line.pop(index)
