from task_3 import merge


def test_from_technical_task():
    iterable_1 = [1, 5, 9]
    iterable_2 = [2, 5]
    iterable_3 = [1, 6, 10, 11]
    excepted_seq = [1, 1, 2, 5, 5, 6, 9, 10, 11]
    assert list(merge(iterable_1, iterable_2, iterable_3)) == excepted_seq


def test_without_iterables():
    assert list(merge()) == list()


def test_with_empty_iterable():
    iterable_1 = [1, 5, 9]
    iterable_2 = []
    iterable_3 = [1, 6, 10, 11]
    excepted_seq = [1, 1, 5, 6, 9, 10, 11]
    assert list(merge(iterable_1, iterable_2, iterable_3)) == excepted_seq


def test_with_all_empty_iterables():
    assert list(merge([], [], [])) == []
