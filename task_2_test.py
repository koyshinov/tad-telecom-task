from pathlib import Path

from task_2 import read_files_in_multiplex_line_by_line


def test_from_technical_task():
    """This test check only functionality

    Test files small, so we in test function keep lines in memory
    :return:
    """
    test_files_dir = Path('files_for_task_2_test')
    multiplex_line_by_line_generator = read_files_in_multiplex_line_by_line(
        str(test_files_dir / 'file_1.txt'),
        str(test_files_dir / 'file_2.txt'),
        str(test_files_dir / 'file_3.txt'),
    )
    output_file = test_files_dir / 'expected_15_lines_of_output.txt'

    output_lines = list()

    for i, line in enumerate(multiplex_line_by_line_generator):
        if i >= 15:
            break

        output_lines.append(line)

    with output_file.open() as f:
        expected_lines = list(line.strip() for line in f.readlines())

    assert output_lines == expected_lines
